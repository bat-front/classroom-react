import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button } from "@mui/material";
import { useDispatch } from "react-redux";

import EditIcon from "@mui/icons-material/Edit";
import VisibilityIcon from "@mui/icons-material/Visibility";
import DeleteIcon from "@mui/icons-material/Delete";
import useFetchAndLoad from "../../hooks/useFetchAndLoad";
import {
  deleteCourse,
  getCourses,
  postCourse,
  updateCourse,
} from "../../services/course.service";
import createImage from "../../services/image.service";
import { API_SCHEME } from "../../constants/api.constants";

import createCoursesListAdapter from "../../adapter/courses.adapter";
import SeparatorBar from "../../components/SeparatorBar";
import Form from "../../components/Form";
import courseFormInputs from "../../components/Form/FormInputs/addCourseFormInputs";
import ErrorCard from "../../components/ErrorCard";
import Modal from "../../components/Modal";
import FilterCoursesList from "../../components/FilterCoursesList";
import addFilterInputs from "../../components/FilterCoursesList/FilterInputs/addInputs";
import { fetchCoursesInit } from "../../redux/reducer/courses";

function Courses() {
  const [toggleForm, setToggleForm] = useState(false);
  const [toggleDelete, setToggleDelete] = useState(false);
  const [courseActive, setCourseActive] = useState({});
  const [courses, setCourses] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");
  const navigate = useNavigate();
  const { callEndpoint } = useFetchAndLoad();
  const dispacth = useDispatch();

  const handleToggle = () => {
    setCourseActive({});
    setToggleForm((c) => !c);
  };

  const handleClickOpen = () => {
    setToggleDelete(true);
  };

  const handleClose = async (id) => {
    setToggleDelete(false);
    if (id) {
      await callEndpoint(deleteCourse(id));
      // if (status === 204) console.log("SUCCESS");
      const updatedList = courses.filter((course) => course.id !== id);
      setCourses(updatedList);
    }
    setCourseActive({});
  };

  const listOptions = [
    {
      title: "View",
      link: ({ id }) => navigate(`course/${id}`),
      icon: <VisibilityIcon />,
      disabled: false,
    },
    {
      title: "Edit",
      link: (course) => {
        setCourseActive(course);
        setToggleForm(true);
        window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      },
      icon: <EditIcon />,
      disabled: toggleForm,
    },
    {
      title: "Delete",
      link: (course) => {
        setCourseActive(course);
        handleClickOpen();
      },
      icon: <DeleteIcon />,
      disabled: false,
    },
  ];

  useEffect(() => {
    const fetchCourses = async () => {
      try {
        const courseList = await callEndpoint(getCourses());
        const coursesData = createCoursesListAdapter(courseList.data);
        setCourses(coursesData);
      } catch (error) {
        setCourses([]);
      }
    };

    fetchCourses();
    // eslint-disable-next-line
  }, []);

  const handleSubmit = async (values) => {
    try {
      const courseName = values["Course Name"];
      let image = "";
      if (values["Course Image"].length !== 0) {
        const formData = new FormData();
        formData.append("files", values["Course Image"][0], image.name);
        image = await callEndpoint(
          createImage("course", `${courseName}`, formData),
        );
      }

      const body = {
        courseName,
        description: values.Description,
        startingDate: values["Start Date"],
        endingDate: values["End Date"],
        image: image.data ? `${API_SCHEME}${image.data}` : "default",
      };

      if (Object.keys(courseActive).length === 0) {
        // POST
        const newCourse = await callEndpoint(postCourse(body));
        const courseData = createCoursesListAdapter([newCourse.data]);
        setCourses([...courses, courseData[0]]);
        dispacth(fetchCoursesInit());
      } else {
        // PUT
        const updatedCourse = await callEndpoint(
          updateCourse(courseActive.id, body),
        );
        const data = createCoursesListAdapter([updatedCourse.data]);
        const tempCourses = courses.map((course) => {
          let returnedCourse = { ...course };
          if (course.id === data[0].id) {
            returnedCourse = { ...data[0] };
          }
          return returnedCourse;
        });

        setCourses(tempCourses);
        dispacth(fetchCoursesInit());
      }
      setCourseActive({});
      setToggleForm(false);
      setErrorMessage("");
    } catch (error) {
      if (error.response.data.length > 0) {
        setErrorMessage(`${error.response.data}`);
      } else {
        setErrorMessage(`${error.response.statusText}`);
      }
    }
  };

  const style = {
    selected: {
      fontSize: "16px",
      fontWeight: "bolder",
    },
    unselected: {
      fontSize: "16px",
      fontWeight: "bolder",
      backgroundColor: "grey",
    },
    delete: {
      fontSize: "16px",
      fontWeight: "bolder",
      backgroundColor: "#d14444",
    },
    cancel: {
      fontSize: "16px",
      fontWeight: "bolder",
    },
  };

  return (
    <Box>
      {errorMessage && <ErrorCard message={errorMessage} />}
      {toggleForm && (
        <Form inputs={courseFormInputs(courseActive)} onSubmit={handleSubmit} />
      )}
      <SeparatorBar title="Courses">
        <Button
          variant="contained"
          style={!toggleForm ? style.selected : style.unselected}
          onClick={handleToggle}
        >
          Register Course
        </Button>
      </SeparatorBar>
      <Box style={{ padding: "1em 1em" }}>
        <FilterCoursesList
          inputs={addFilterInputs}
          originalList={courses}
          listOptions={listOptions}
          toggleForm={toggleForm}
        />
      </Box>
      <Modal
        open={toggleDelete}
        onClose={handleClose}
        title="Delete Course"
        message={`Are you sure to delete course ${courseActive.courseName}?`}
      >
        <Box
          style={{
            display: "flex",
            justifyContent: "space-around",
            padding: "15px",
          }}
        >
          <Button
            variant="contained"
            onClick={() => handleClose()}
            style={style.cancel}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            onClick={() => handleClose(courseActive.id)}
            style={style.delete}
          >
            Delete
          </Button>
        </Box>
      </Modal>
    </Box>
  );
}

export default Courses;
