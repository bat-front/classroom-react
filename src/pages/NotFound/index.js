import { Box, Typography } from "@mui/material";
import React from "react";

function NotFound() {
  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      minHeight="100px"
    >
      <Typography variant="h5">Page Not Found!</Typography>
    </Box>
  );
}

export default NotFound;
