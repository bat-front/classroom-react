import StudentPageContextProvider from "./StudentPageContextProvider";
import { useStudentPageContext } from "./studentPageContext";

export { StudentPageContextProvider, useStudentPageContext };
