import { BASE_API_URL, HEADERS } from "./api.constants";

const options = {
  GET_SUBJECTS_BY_COURSE_ID: (courseId) => {
    return {
      method: "GET",
      url: `${BASE_API_URL}/Subject/${courseId}`,
      headers: HEADERS,
    };
  },
  POST_SUBJECT: (subject) => {
    return {
      method: "POST",
      url: `${BASE_API_URL}/Subject`,
      headers: HEADERS,
      data: subject,
    };
  },
};

export default options;
