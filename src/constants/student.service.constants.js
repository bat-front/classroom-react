import { BASE_API_URL, HEADERS } from "./api.constants";

const options = {
  GET_STUDENT_BY_COURSE_ID: (courseId) => {
    return {
      method: "GET",
      url: `${BASE_API_URL}/Student/ByCourseId?courseId=${courseId}`,
      headers: HEADERS,
    };
  },
  GET_UNREGISTERED_STUDENTS: () => {
    return {
      method: "GET",
      url: `${BASE_API_URL}/Student?onlyNotRegistered=true`,
      headers: HEADERS,
    };
  },
  UPDATE_STUDENT: (register, id, courseId) => {
    if (register) {
      return {
        method: "PUT",
        url: `${BASE_API_URL}/Student/${id}?courseId=${courseId}`,
      };
    }
    return {
      method: "PUT",
      url: `${BASE_API_URL}/Student/${id}`,
    };
  },
  GET_STUDENTS: () => {
    return {
      method: "GET",
      url: `${BASE_API_URL}/Student`,
      headers: HEADERS,
    };
  },
  POST_STUDENT: (student) => {
    return {
      method: "POST",
      url: `${BASE_API_URL}/Student`,
      data: student,
      headers: HEADERS,
    };
  },
};

export default options;
