import { BASE_API_URL, HEADERS } from "./api.constants";

const options = {
  POST_IMAGE: (folder, fileName, image) => {
    return {
      method: "POST",
      url: `${BASE_API_URL}/Images/upload?folder=${folder}&fileName=${fileName}`,
      headers: { "Content-Type": "multipart/form-data", ...HEADERS },
      data: image,
    };
  },
};

export default options;
