import React from "react";
import PropTypes from "prop-types";
import { Alert, Box, Typography } from "@mui/material";

function ErrorCard({ message }) {
  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      minHeight="70px"
      style={{
        color: "white",
        backgroundColor: "#d73b3e",
      }}
    >
      <Alert severity="error">
        <Typography variant="h6">{message}</Typography>
      </Alert>
    </Box>
  );
}

ErrorCard.propTypes = {
  message: PropTypes.string.isRequired,
};

export default ErrorCard;
