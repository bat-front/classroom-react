import React from "react";
import PropTypes from "prop-types";
import {
  Avatar,
  Box,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Icon,
  IconButton,
  Menu,
  MenuItem,
  Typography,
} from "@mui/material";
import { Link } from "react-router-dom";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import PeopleAltIcon from "@mui/icons-material/PeopleAlt";
import ListAltIcon from "@mui/icons-material/ListAlt";
import CircleIcon from "@mui/icons-material/Circle";
import { useDispatch } from "react-redux";
import {
  displaySubjectsFalse,
  displaySubjectsTrue,
} from "../../redux/reducer/students";

function CourseCard({ course, listOptions }) {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const dispatch = useDispatch();

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  let statusColor = "";
  switch (course.status) {
    case "Active":
      statusColor = "#4fd92d";
      break;
    case "Finished":
      statusColor = "#c51515";
      break;
    default:
      statusColor = "#f3a026";
      break;
  }

  return (
    <Card>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe">
            {course.courseName.substring(0, 1)}
          </Avatar>
        }
        action={
          <IconButton aria-label="settings" onClick={handleOpenNavMenu}>
            <MoreVertIcon />
          </IconButton>
        }
        title={course.courseName}
        subheader={course.startingDate}
      />
      <Menu
        id="menu-appbar"
        anchorEl={anchorElNav}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        open={Boolean(anchorElNav)}
        onClose={handleCloseNavMenu}
        sx={{
          display: { xs: "block", sm: "block" },
        }}
      >
        {listOptions.map(({ title, link, icon, disabled }) => (
          <MenuItem
            key={title}
            onClick={handleCloseNavMenu}
            disabled={disabled}
          >
            <Typography
              textAlign="center"
              onClick={() => {
                link(course);
              }}
              style={{
                textDecoration: "none",
                color: "grey",
                display: "flex",
                gap: ".5em",
              }}
            >
              <Icon>{icon}</Icon>
              {title}
            </Typography>
          </MenuItem>
        ))}
      </Menu>
      <CardMedia
        component="img"
        height="194"
        image={course.image}
        alt={course.courseName}
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {course.description}
        </Typography>
      </CardContent>
      <CardActions
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Box>
          <IconButton
            onClick={() => {
              dispatch(displaySubjectsFalse());
            }}
          >
            <Link
              to={`/course/${course.id}`}
              style={{
                textDecoration: "none",
                color: "grey",
              }}
            >
              <PeopleAltIcon />
            </Link>
          </IconButton>
          <IconButton
            onClick={() => {
              dispatch(displaySubjectsTrue());
            }}
          >
            <Link
              to={`/course/${course.id}`}
              style={{
                textDecoration: "none",
                color: "grey",
              }}
            >
              <ListAltIcon />
            </Link>
          </IconButton>
        </Box>
        <Box
          style={{
            marginRight: "10px",
            display: "flex",
            alignItems: "center",
          }}
        >
          <Box>
            <Typography fontSize="small">Status</Typography>
            <Typography
              sx={{
                mr: 2,
                fontFamily: "monospace",
                fontWeight: 700,
                letterSpacing: ".2rem",
                color: "inherit",
                textDecoration: "none",
              }}
            >
              {course.status}
            </Typography>
          </Box>
          <Icon>
            <CircleIcon fontSize="small" style={{ color: statusColor }} />
          </Icon>
        </Box>
      </CardActions>
    </Card>
  );
}

CourseCard.propTypes = {
  course: PropTypes.PropTypes.shape({
    id: PropTypes.number.isRequired,
    courseName: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    startingDate: PropTypes.string.isRequired,
    endingDate: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
  }).isRequired,
  listOptions: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      link: PropTypes.func.isRequired,
      icon: PropTypes.node.isRequired,
      disabled: PropTypes.bool.isRequired,
    }),
  ).isRequired,
};

export default CourseCard;
