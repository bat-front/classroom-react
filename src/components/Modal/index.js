import React from "react";
import PropTypes from "prop-types";
import { Box, DialogTitle, Dialog } from "@mui/material";
import Typography from "@mui/material/Typography";

function Modal({ onClose, open, title, message, children }) {
  const handleClose = () => {
    onClose();
  };

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>{title}</DialogTitle>
      <Box
        style={{
          marginRight: "10px",
          display: "flex",
          justifyContent: "space-around",
          padding: "15px",
        }}
      >
        <Typography>{message}</Typography>
      </Box>
      {children}
    </Dialog>
  );
}

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default Modal;
