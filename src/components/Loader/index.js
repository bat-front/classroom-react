import React from "react";
import "./loader.css";

function Loader() {
  return (
    <div className="app__loader">
      <div className="lds-facebook">
        <div />
        <div />
        <div />
      </div>
    </div>
  );
}

export default Loader;
