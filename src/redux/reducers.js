import { combineReducers } from "redux";
import coursesReducer from "./reducer/courses";
import countriesReducer from "./reducer/countries";
import studentsReducer from "./reducer/students";

const rootReducer = combineReducers({
  courses: coursesReducer,
  countries: countriesReducer,
  students: studentsReducer,
});

export default rootReducer;
