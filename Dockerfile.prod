FROM node:14-alpine AS build
# Add a work directory
WORKDIR /app
COPY package.json .
COPY yarn.lock .
RUN yarn install
ARG REACT_APP_SERVER_API
ARG REACT_APP_SERVER_SCHEME
ENV REACT_APP_SERVER_API=$REACT_APP_SERVER_API
ENV REACT_APP_SERVER_SCHEME=$REACT_APP_SERVER_SCHEME
# Copy app files
COPY . .
RUN yarn build

# Prepare nginx
FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
